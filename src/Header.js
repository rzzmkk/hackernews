import React from "react";
import './Header.css';

function Header(){
    return(
        <header>
            <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contacts</a></li>
            </ul>
        </header>
    )
}

export default Header;